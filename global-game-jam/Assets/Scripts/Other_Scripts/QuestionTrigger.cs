﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionTrigger : MonoBehaviour
{

    public Text qText;

    // Start is called before the first frame update
    void Start()
    {
        qText.enabled = false;
    }

    // Update is called once per frame
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Player") { qText.enabled = true; }
    }

    void OnTriggerExit2D(Collider2D other) { qText.enabled = false; }

}
