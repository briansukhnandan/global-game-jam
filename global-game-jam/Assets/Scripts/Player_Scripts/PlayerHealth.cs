﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/* 

    Takes care of healthCounter/respawn updates.

*/

public class PlayerHealth : MonoBehaviour {

    public static int healthCounter;
    public static int ship_part_counter;
    private bool invincible = false;
    public Text healthText = null;
    public Text ship_part_text = null;
    public Text WinText = null;
    public Text npcText = null;
    public Text gameOverText = null;
    private Scene scene;
    public GameObject deathDoor;

    void Start() {
        healthCounter = 250;
        ship_part_counter = 0;

        healthText.text = "Health: " + healthCounter.ToString();
        healthText.enabled = true;

        ship_part_text.text = "Ship Parts: " + ship_part_counter.ToString();
        ship_part_text.enabled = true;

        WinText.enabled = false;
        gameOverText.enabled = false;

        this.scene = SceneManager.GetActiveScene();
    }

    void Update() {
        // Handles invincibility when enemy is hit or state is changed.
        if (invincible) StartCoroutine(Invulnerability());

        if (healthCounter <= 0) {
            
            gameOverText.enabled = true;
            respawn();

        }

    }

    private void respawn() { SceneManager.LoadScene(this.scene.name); }

    // Handles Collision
    private void OnTriggerEnter2D(Collider2D collision) {

        // If the player is hit by an enemy wielding the GoombaScript:
        if (collision.CompareTag("Enemy")) {

            healthCounter -= 3;
            healthText.text = "Health: " + healthCounter.ToString();
            invincible = true;

        }

        if (collision.CompareTag("Boss")) {

            healthCounter -= 7;
            healthText.text = "Health: " + healthCounter.ToString();
            invincible = true;

        }

        if (collision.CompareTag("Fire_Enemy")) {

            healthCounter -= 15;
            healthText.text = "Health: " + healthCounter.ToString();
            invincible = true;

        }

        if (collision.CompareTag("correct_ans_block")) {
            Destroy(deathDoor);
        }

        if (collision.CompareTag("ship_part")) {
            ship_part_counter = ship_part_counter+1;
            ship_part_text.text = "Ship Parts: " + ship_part_counter.ToString();
        }

        
        if (collision.CompareTag("DeathDoor")) {
            respawn();
        }

        if (collision.CompareTag("food")) {
            healthCounter += 30;
            healthText.text = "Health: " + healthCounter.ToString();
        }

        if (collision.CompareTag("npc")) {        
            npcText.enabled = true;
        }

        if (collision.CompareTag("incorrect_ans_block")) {
            respawn();
        }


        if (collision.CompareTag("ship")) {
            if ((ship_part_counter >= 5)) {
                WinText.enabled = true;
            }
        }

    }

    void OnTriggerExit2D(Collider2D other) { npcText.enabled = false; }

    // Invulnerability Iterator
    IEnumerator Invulnerability() {
        yield return new WaitForSeconds(3f);
        invincible = false;
    }

}
