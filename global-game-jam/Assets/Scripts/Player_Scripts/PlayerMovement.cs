﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public float avgSpeed = 10f;
    public float horizontalMovement = 0f;

    public bool attackState;

    private Animator animationHandler;
    private SpriteRenderer mySpriteRenderer;

    public bool facingN;
    public bool facingW;
    public bool run_anim;

    // Start is called before the first frame update
    void Start()
    {
       animationHandler = GetComponent<Animator>();
       mySpriteRenderer = GetComponent<SpriteRenderer>(); 
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetButtonDown("Jump"))
        {

            attackState = true;
            avgSpeed = 2f;
        }

        if (Input.GetButtonUp("Jump"))
        {

            attackState = false;
            avgSpeed = 5f;
        }

        if (Input.GetAxisRaw("Horizontal") > 0.1f || Input.GetAxisRaw("Horizontal") < -0.1f)
        {

            //Will be positive or negative based on input, no need to add an extra if statement.
            transform.Translate(new Vector3(Input.GetAxisRaw("Horizontal") * avgSpeed * Time.deltaTime, 0f, 0f));

            run_anim = true;

            if (Input.GetAxisRaw("Horizontal") < -0.1f)
            {

                facingW = true;
                mySpriteRenderer.flipX = true;

            }

            if (Input.GetAxisRaw("Horizontal") > 0.1f)
            {

                facingW = false;
                mySpriteRenderer.flipX = false;
            }


        }

        else if (Input.GetAxisRaw("Vertical") > 0.1f || Input.GetAxisRaw("Vertical") < -0.1f) {

            transform.Translate(new Vector3(0f, Input.GetAxisRaw("Vertical") * avgSpeed * Time.deltaTime, 0f) );

            run_anim = true;

            if (Input.GetAxisRaw("Vertical") > 0.1f)
            {
                facingN = true;

            }

            if (Input.GetAxisRaw("Vertical") < 0.1f)
            {

                facingN = false;

            }


        }

        else {
            run_anim = false;
        }

        animationHandler.SetBool("Run", run_anim);
        animationHandler.SetBool("AttackState", attackState);

        
    }

    void OnTriggerEnter2D() {

        
    }

}
